[![coverage report](https://gitlab.com/novarx/fwebt/badges/master/coverage.svg)](https://gitlab.com/novarx/fwebt/-/commits/master)

# Wichtigste Links
[🐍 **Deployed Snake**](https://snake-fwebt.web.app/)

[📝 **Dokumentation**](https://gitlab.com/novarx/fwebt/builds/artifacts/master/file/doc/dokument.pdf?job=documentation)

[🖥 **Präsentation**](https://gitlab.com/novarx/fwebt/builds/artifacts/master/file/doc/slides.pptx?job=documentation)

[👾 **Gitlab Repository**](https://gitlab.com/novarx/fwebt/)

[🌗 **Gitlab Repository Spiegelung FFHS**](https://git.ffhs.ch/shankar.ram/webt/-/tree/dev-bsc.inf.2016.salomon-sterchi)

# SNAKE
Im Rahmen des Moduls _FWebT_ implementieren wir ein [Snake-Game](https://en.wikipedia.org/wiki/Snake_(video_game_genre))
mithilfe des JavaScript-Frameworks [React](https://reactjs.org/).

Hauptfokus soll auf dem Umgang mit JavaScript und React sein. Wobei natürlich unser Anspruch ist, ein voll 
funktionsfähiges Snake zu bauen.


# Semesterarbeit
Teil dieses Moduls ist eine Semesterarbeit bei der die Studierenden ein interaktives Spiel entwickeln.
Die Semesterarbeit erstreckt sich über das gesamte Semester und wird in Gruppen von maximal zwei Studierenden bearbeitet.
Die Arbeit wird bewertet und zählt 40% zur Gesamtnote. Die Definition des zu entwickelnden Spiels liegt in der Verantwortung der Gruppe.
Das Ziel ist jedoch dass die gelernten Konzepte und Technologien angewandt werden. Die Bewertung erfolgt nach folgendem Schema: 
- Vollständigkeit der Implementierung (sind alle Funktionen implementiert): 30 %
- Korrektheit (sind alle Funktionen korrekt implementiert): 30%
- Fehlerbehandlung: 20%
- Präsentation und Demo: 20%

## Kriterien
Die zu erfüllenden Kriterien der Semesterarbeit sind zusammengefasst laut Moodle:
- Das Projekt soll durch die vorgegebene CI/CD Pipeline laufen
- die zu realisierenden Funktionalitäten beschreiben (im Code)
- Verwenden Sie relevante Module in Ihrem Eigenprojekt
- Erstellen Sie einen Beschrieb der Modularisierung
- Erstellen Sie entsprechende Unit-Tests für die umgesetzten Funktionalitäten
- Clientseitige Objekte sollen in einem Backend gespeichert werden können
- Erstellen Sie die Verbindung zu einem bestehenden Backend (in einem neuen Ordner backend)
- Erstellen Sie eine Präsentation für die letzte PVA
