---
title: "Snake with React"
author: Paul Salomon & Daniel Sterchi
date: 09.05.2020
header-includes:
    - \usepackage{xcolor}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \fancyhead[L]{P.Salomon \& D. Sterchi}
    - \fancyhead[C]{BSc INF 2016.BE1}
    - \fancyhead[R]{FS20}
geometry: "left=3cm,right=3cm,top=3.5cm,bottom=2cm"
---

\pagenumbering{gobble}
\setcounter{tocdepth}{3}
\tableofcontents

\newpage
\pagenumbering{arabic}

