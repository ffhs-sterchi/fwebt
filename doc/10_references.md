# Links
- Deployed Snake - https://snake-fwebt.web.app/
- Dokumentation - https://gitlab.com/novarx/fwebt/builds/artifacts/master/file/doc/dokument.pdf?job=documentation
- Präsentation - https://gitlab.com/novarx/fwebt/builds/artifacts/master/file/doc/slides.pptx?job=documentation
- Gitlab Repository - https://gitlab.com/novarx/fwebt/
- Gitlab Repository Spiegelung FFHS - https://git.ffhs.ch/shankar.ram/webt/-/tree/dev-bsc.inf.2016.salomon-sterchi

[^react]: https://reactjs.org/
[^Subject]: https://git.ffhs.ch/shankar.ram/webt/-/tree/dev-bsc.inf.2016.salomon-sterchi/snake/src/_model/subject.ts
[^rxjs]: https://rxjs-dev.firebaseapp.com/
[^app]: https://snake-fwebt.web.app/
[^Source-Code]: https://git.ffhs.ch/shankar.ram/webt/-/tree/dev-bsc.inf.2016.salomon-sterchi
[^.gitlab-ci.yaml]: https://git.ffhs.ch/shankar.ram/webt/-/blob/dev-bsc.inf.2016.salomon-sterchi/.gitlab-ci.yml
[^Snake]: https://en.wikipedia.org/wiki/Snake_(video_game_genre)
