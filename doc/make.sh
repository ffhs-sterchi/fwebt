# When /doc ist different from Master, check if doc can be converted
git diff --quiet HEAD master -- ./ ||
  docker run --rm -v "$(pwd -W):/data" \
novarx/pandoc \
"00_title.md" "01_documentation.md" "02_summary.md" "10_references.md" -o dokument.pdf \
-V fontsize=12pt \
-V papersize=a4paper \
--pdf-engine=xelatex \
--filter=pandoc-plantuml &&
  docker run --rm -v "$(pwd -W):/data" \
novarx/pandoc \
"slides.md" \
-o slides.pptx \
--reference-doc "template.pptx"
