\newpage
# Summary
Snake in React zu entwickeln war ein interessantes Projekt. Wir konnten uns so wirklich mit der Library beschäftigen
anhand konkreter Beispiele. Um das UI zu strukturieren, bietet React doch recht praktische Tools, welche sich
insbesondere auf das Rendern von HTML Code beziehen. So wird jedes Mal, wenn der sogenannte _State_ einer _Component_ 
angepasst wird auch gleich das UI updated.

Ansonsten bietet einem React allerdings nicht viele Richtlinien oder Vorgaben. Auch sehr hilfreiche Tools wie RxJS
[^rxjs] sind nicht bereits nativ integriert. Da es sich allerdings um eine UI Library handelt, ist das auch nicht weiter
tragisch.

Die Logik des Spiels, sowie die gesamte Spiel-Status-Verwaltung haben wir schlussendlich auch ausserhalb von React
umgesetzt. In einfachen TypeScript Klassen konnten wir so die einzelnen Abhängigkeiten sauber strukturieren. 

Alles in allem war das Projekt sehr lehrreich, bot doch einige Hürden und eine nicht zu vernachlässigende Lernkurve.

![Fertiges Snake-Game](./res/snake.png){width=60%}
