# Einleitung
Im Rahmen des Faches _Fortgeschrittene Web-Technologien_ (FWebT) haben wir im Frühlingssemester 2020 eine React[^react]
App des beliebten Spiels _Snake_[^Snake] erstellt.

Ziel dieser Semester begleitenden Arbeit war es insbesondere, sich mit fortgeschrittenen Technologie und Techniken der
Web-Entwicklung zu beschäftigen. So haben wir uns ein Spiel ausgesucht, welches beide kannten, nicht den Aufwand
sprengen würde aber dennoch etwas Fleisch am Knochen hat - Snake also.

# Vorgehen
Da wir in einer Gruppe (also zu zweit) arbeiten, haben wir uns als erstes Gedanken darüber gemacht, was für Funktionen
bzw. Arbeiten benötigt werden, um ein funktionierendes Snake zu haben.

Diese Arbeiten haben wir zu Beginn alle in einem Gitlab Repository als Issues erfasst. Die Issues wurden auch so erfasst
bzw. "_geschnitten_", dass ein gleichzeitiges Arbeiten am Projekt möglich ist. Um das zu erreichen haben wir uns die
Aufgaben so bereitgelegt, dass sie möglichst wenige Abhängigkeiten untereinander haben. Wo Abhängigkeiten unvermeidbar
waren haben wir das explizit deklariert um nicht mitten in der Entwicklung auf Überraschungen bzw. nicht implementierte
Erwartungen zu stossen.

![Development-Board](./res/dev-board.png)

# Funktionen
Die obengenannten Anforderungen führen wir hier noch aus.

## Funktionale Anforderungen
### Food soll zufällig gesetzt werden
Es soll zufällig ein ```Square``` mit z.B. ```hasFood``` markiert werden.
Durch diese Markierung soll das Quadrat eine spezielle Farbe erhalten, z.B. Grün.
Ob ein Quadrat Essen enthält oder nicht soll nebst dem Quarat selbst, mindestens auch noch in ```Game``` bekannt sein.
```Game``` soll also den Status der Quadrate kennen.

### Snake soll sich bewegen
Die Schlange soll sich vorwärts bewegen können, wobei die Schlange vorerst nur aus einem Quadrat bestehen soll. Und zusätzlich soll Sie auch nicht gesteuert werden können
Ein Quadrat muss also wissen, das die Schlange aktuell auf dem Feld ist, mit z.B. ```hasSnake```. Wodurch sich die Farbe des Quadrates ändern soll, z.B. Orange.
Die Schlange soll sich in einem definierten Tempo in eine Richtung bewegen. Wenn die Schlange über die Ebene hinaus gehen würde, soll Sie in derselben Reihe/Zeile auf der gegenüberliegenden Seite wieder weiter gehen.

### Snake soll einen Körper haben
Die Schlange soll Initial einen Körper der Länge 3 haben, es sollen also drei Quadrate mit ```hasSnake``` geflagt sein. Wenn sich die Schlange also vorwärts bewegt, wird also immer der ganze Körper mitgezogen werden
Die Länge des Körpers soll nicht auf 3 beschränkt sein, wird aber im Rahmen dieses Issues auch nicht grösser werden als der Initial Zustand. Auch kann die Schlange immer noch nicht die Richtung ändern.

### Snake soll Food konsumieren
Wenn die Schlange ein Feld erreicht welches Essen enthält, soll sie nach dem kompletten Durchlaufen dieses Feldes um ein Feld länger werden.
Das Essen soll jedoch beretis beim erreichen des Schlangenkopfes von diesem Feld verschwinden und zufällig an einem anderen Ort erscheinen.
Ein möglicher Ablauf des _essens_ könnte, wenn man nur eine Zeile beachtet, wie folgt aussehen (# = Schlange, X
 = Essen):

```
1. ###  X
2.  ### X
3.   ###X
4.    ###      X
5.     ###     X
6.      ###    X
7.       ####  X
8.        #### X
...
```

### Snake soll bei Kollision mit Wand nicht mehr weitergehen
(noch nicht Implementiert)

Optional soll eine _Kollision_ ausgelöst werden, wenn der Kopf der Schlange über die Ebene hinaus gehen würde. Dadurch würde auch ein "Game Over" ausgelöst werden.
Optional heisst, es soll möglich sein diese Option z.B. vor beginn des Spiels anzuwählen.

### Snake soll bei Kollision mit sich selbst nicht mehr weiter gehen
Wenn der Kopf der Schlange einen anderen Teil von sich selbst berühren würde, soll sie sich nicht mehr weiter bewegen.


### Score soll die aktuelle Länge von Snake anzeigen
Der ```Score``` component soll immer die aktuelle Länge der Schlange anzeigen. Diese repräsentiert die Punktzahl
, welche ein Spieler erreicht hat.

**Bemerkung der Implementierung**: `Score` zeigt nun nicht die Länge der Schlange an, sonder die Anzahl bereits
konsumierter Essen. Dazu gibt es im `GameHandler` ein `score` Subject[^Subject] das ist ein Attribut, welches mit einer
Methode `.subscribe()` abonniert werden kann.

### Game soll bei Kollision "Game Over" auslösen
Wenn eine Kollision entdeckt wird, soll der Text "Game Over" markant erscheinen. Zusätzlich soll es einen Button geben um das Spiel zu resetten.
Ob der Reset-Button auch angezeigt werden soll, wenn _Game Over_ noch nicht ausgelöst wurde, ist zu definieren
/ bestimmen.


### Snake soll die Richtung ändern können
Die Schlange soll mittels Pfeiltasten und/oder _W, A, S, D_ gesteuert werden können.
Wenn sich die Schlange z.B. von unten nach oben bewegt und die Linke Pfeiltaste gedrückt wird, soll sich der Kopf der Schlange mit dem nächsten Tick von Rechts nach Links bewegen. Der Körper wird dan Tick für Tick nachgezogen.
Es soll nicht möglich sein, die Richtung so zu ändern, dass die Schlange sich genau in die entgegengesetzte Richtung bewegt. Wenn z.B. die aktuelle Richtung unten->oben ist, so sollen nur die Tasten _Links_ und _Rechts_ eine Wirkung haben.
Auch hier wieder beachten, dass bei einem ""Übertreten"" der Ebene die Schlange auf der anderen Seite wieder auftauchen soll.

### "Game Over" soll den Punktestand persistieren
Wenn ein Game Over ausgelösst wird soll der aktuelle Punktestand auf einem Server persistiert werden können.

**Bemerkung der Implementierung**: 

Als Backend haben wir Firebase verwendet. So müssen wir uns nicht noch zusätzlich um ein Backend kümmern, sondern können
direkt auf vorgefertigte Konstrukte und Funktionen zugreifen.

Firebase kann via NPM einfach installiert und verwendet werden. Um z.B. ein Dokument (entspricht einem Datensatz) zu
erhalten, kann z.B. folgende Funktion ausgeführt werden:
```javascript
firebase.firestore().doc('aCollection/aDocumentId').get()
    .then(data => console.log('recived data', data));
```

So wird das Dokument _aDocumentId_ in der Firebase Collection _aCollection_ abgerufen. Mit dem `.then()` dahinter lässt
bereits feststellen, dass dort ein `Promise` zurückkommt, wodurch asynchron das `console.log` aufgerufen wird, sobald
die Daten erhalten wurden.

## Nicht Funktionale Aufgaben
### Testing Suite aufetzen
Das Projekt soll mittels Unit-Tests und einigen Integrations-Tests getestet werden können.

### CI/CD anpassen für React
Es sollen Build Jobs bestehen um die integrität des React-Projektes zu sichern.

Dabei soll eine Testing-Suit laufen welche u.a. auch die Testabdeckung prüft, zudem soll das Projekt gelinted werden
und schlussendlich "gebuilded" werden können.

### Performance optimieren
<sup>Ist erst im Verlauf des Projektes erfasst</sup>

Es werden immer alle Felder neu geladen bei einem Tick. Das verlangsamt die App extrem.
Die Codepassage aus _app.tsx_ könnte vielleicht in den _GameHandler_ gezogen werden:
```javascript
setInterval(() => {
    this.setState({
        ...this.gameHandler.tick()
    });
}, 100);
```

Mit einem Observer Pattern können dann die einzelnen z.B. _Square_ Änderungen auf einzelne _Fields_ abonieren.

## Known Bugs
Es gibt bereits einige Bugs, die wir kennen, jedoch noch nicht bearbeiten konnten.

### Voller Richtungswechsel in einem Tick
"Während einem _tick_ kann die richtung um 180° gedreht werden. Wenn man z.B. aktuell die Richtung _up()_ hat, danach
schnell hintereinander die Pfeiltasten gegen Links und gleich nach unten drückt, läuft die Schlange in sich selbst.
Das sollte verhindert werden."

### Memory leak in Square
In `Square` wird die Subscription auf das Field nicht unsubscribed.

# Code-Overview
Wie bereits erwähnt, haben wir das Game mit React entwickelt, das Framework haben wir gewählt, weil wir beide noch
keine Erfahrung damit hatten.

Das Projekt haben wir mit dem React Befehl `npx create-react-app` erstellt, was einem gleich die benötigten
Dependencies mit NPM initialisiert und eine minimale Struktur vorgibt. Von der Basisstruktur aus haben wir
schliesslich begonnen unsere Ordner- und File-Struktur aufzusetzen.

Anfänglich hatten wir eine sehr flache Ordnung, wobei der erste Wurf ca. so ausgesehen hatte:
```{.plantuml height=30%}
@startmindmap
* src
** game
** plane
** square
** score
** ...
@endmindmap
```

Da wir aber damit rechneten, dass es wahrscheinlich noch weitere Module geben könnte und aber auch um die 
Zugehörigkeiten der einzelnen React komponenten etwas besser zu erkennen, sieht die Struktur nun wie folgt aus:

```{.plantuml width=60%}
@startmindmap
* src
** _model
*** ...
** _service
*** ...
** game
*** game-over-message
*** score
*** plane
**** square
@endmindmap
```

So ist rasch zu erkennen, dass das "_game_" eine "_plane_" hat (also das "Spielfeld") und diese Ebene besteht aus
"_square_'s" also Quadraten.
 
Zusätzlich gibt es z.B. noch *_model* worin sich Klassen und Interfaces befinden. Die dienen hauptsächlich zur
typisierung von Variablen und Funktionen. Wir haben uns nämlich während dem Entwickeln dazu entschieden React mit
Typescript zu verwenden. Da die Typisierung doch eine enorme Hilfe ist.

Für die erwähnte Testabdeckung streben wir folgende Werte an, welche auch Teil der Pipeline sind. Wird also der 
gewünschte Threshold nicht erreicht, schlägt auch die Pipeline fehl.
```json
    "coverageThreshold": {
      "global": {
        "lines": 90,
        "functions": 90,
        "statements": 90,
        "branches": 90
      }
    }
```

Zusätzlich zur Testabdeckung wollen wir auch eine einheitliche Formatierung unseres Codes haben, wozu wir ESlint
verwenden. Auch hier, sollte es Fehler beim _linting_ geben, schlägt die Pipeline fehl.
 
 Der genaue Aufbau der Pipeline findet sich im _.gitlab-ci.yaml_[^.gitlab-ci.yaml].
Implementierungsdetails bzw. der Source-Code[^Source-Code]
findet sich ebenfalls im Gitlab der FFHS, bzw. zusätzlich in einem
privaten Repository auf Gitlab selbst.

# Deployment
Unser Spiel haben wir auf _Firebase_ deployed und ist somit erreichbar unter snake-fwebt.web.app[^app].

Das Deployment wird bei jedem Push auf den Master-Branch in unserem Gitlab Repository ausgeführt. So kann
sichergestellt werden, dass der Build und die Tests erfolgreich gemacht wurden.

Firebase haben wir gewählt, weil das deployment auf einfachste Weise erledigt werden kann. Ein CLI-Command 
`firebase deploy` reicht bereits aus um das gesamte mit `npm run build` erstellte Artefakt zu deployen.

