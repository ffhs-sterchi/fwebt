---
title: "Snake with React"
author: Paul Salomon & Daniel Sterchi
---

# Projekt
- Snake
- React
    - Test-Driven-Development
    - TypeScript & SCSS
- Backend - Firestore
- Gitlab
    - Build-Pipeline
    - Continuous Integration
    - Continuous Deployment (Firebase)

# Funktionen
- Food soll zufällig gesetzt werden
- Snake 
    - soll sich bewegen
    - soll einen Körper haben
    - soll Food konsumieren
    - soll bei Kollision mit sich selbst nicht mehr weiter gehen
    - soll die Richtung ändern können
- Game
    - soll Score anzeigen
    - soll bei Kollision _Game Over_ auslösen und den Punktestand persistieren

# Bugs / Pitfalls
- Voller Richtungswechsel in einem Tick
- Memory leak in Square

# Demo
SPA auf Firebase - [snake-fwebt.web.app](https://snake-fwebt.web.app/)
