import '@testing-library/jest-dom';
import {EventHandler} from './event-handler';

describe('EventHandler', () => {
    const map = {};
    beforeAll(() => {
        window.addEventListener = jest.fn((eventName, callback) => {
            map[eventName] = callback;
        });
    });

    it('should listen to keydown events', () => {
        EventHandler.getHandler();
        EventHandler.getHandler();
        expect(window.addEventListener).toHaveBeenCalledTimes(1);
        expect(Object.keys(map)).toContain('keydown');
    });

    it('should create a EventHandler singleton', () => {
        const handler = EventHandler.getHandler();
        expect(handler).toBeTruthy();
        const handler2 = EventHandler.getHandler();
        expect(handler2).toBe(handler);
    });

    it('should be possible to subscribe to keydown', () => {
        let key = '';
        const handler = EventHandler.getHandler();
        const callback = (event: KeyboardEvent): void => {
            key = event.key;
        };

        handler.subscribeKeydown(callback);

        map['keydown'](new KeyboardEvent('onkeydown', {key: 'W'}));
        expect(key).toEqual('W');
    });

});
