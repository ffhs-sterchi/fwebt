export class EventHandler {
    private static handler;
    private subscriptions = {
        keydown: [] as ((event: KeyboardEvent) => void)[]
    };

    private constructor() {
        window.addEventListener('keydown', event => {
            this.subscriptions.keydown.forEach(callback => {
                callback(event);
            });
        });
    }

    static getHandler(): EventHandler {
        if (!EventHandler.handler) {
            EventHandler.handler = new EventHandler();
        }
        return EventHandler.handler;
    }

    subscribeKeydown(callback: (event: KeyboardEvent) => void) {
        this.subscriptions.keydown.push(callback);
    }

}
