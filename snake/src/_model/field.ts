export class Field {
    id: number;
    hasFood: boolean;
    hasSnake: boolean;
    _observers: ((Field) => void)[] = [];

    next(hasFood: boolean, hasSnake: boolean) {
        this.hasFood = hasFood;
        this.hasSnake = hasSnake;
        this._observers.forEach(callback => callback(this));
    }

    subscribe(callback: (Field) => void) {
        this._observers.push(callback);
    }
}
