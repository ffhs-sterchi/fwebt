import '@testing-library/jest-dom';
import {TestUtil} from '../testing/util';

describe('Field', () => {
    it('is possible to subscribe to change', () => {
        const field = TestUtil.aField();
        const callbackSpy = jasmine.createSpy();
        field.subscribe(callbackSpy);

        field.next(true, true);

        expect(field.hasFood).toEqual(true);
        expect(field.hasSnake).toEqual(true);
        expect(callbackSpy).toHaveBeenCalledTimes(1);
        expect(callbackSpy).toHaveBeenCalledWith(field);
    });
});
