import '@testing-library/jest-dom';
import {Snake} from './snake';

describe('Game as Component', () => {
    let snake: Snake;

    beforeEach(() => snake = new Snake(5));

    it('should create', () => {
        expect(snake).toBeTruthy();
    });

    it('should get coordinates of number correct', () => {
        expect(snake.numberToCoordinates(0)).toEqual({col: 0, row: 0});
        expect(snake.numberToCoordinates(5)).toEqual({col: 0, row: 1});
        expect(snake.numberToCoordinates(12)).toEqual({col: 2, row: 2});
        expect(snake.numberToCoordinates(21)).toEqual({col: 1, row: 4});
        expect(snake.numberToCoordinates(24)).toEqual({col: 4, row: 4});
        expect(snake.numberToCoordinates(25)).toEqual(null);
        expect(snake.numberToCoordinates(-1)).toEqual(null);
    });

    it('should get number of coordinates correct', () => {
        expect(snake.coordinatesToNumber({col: 0, row: 0})).toEqual(0);
        expect(snake.coordinatesToNumber({col: 0, row: 1})).toEqual(5);
        expect(snake.coordinatesToNumber({col: 2, row: 2})).toEqual(12);
        expect(snake.coordinatesToNumber({col: 1, row: 4})).toEqual(21);
        expect(snake.coordinatesToNumber({col: 4, row: 4})).toEqual(24);
        expect(snake.coordinatesToNumber({col: 5, row: 5})).toEqual(null);
        expect(snake.coordinatesToNumber({col: 9, row: 5})).toEqual(null);
    });

    it('should get positions as string', () => {
        let expectedString = '.....\n' +
            '..#..\n' +
            '..#..\n' +
            '..#..\n' +
            '.....';
        snake.move();
        expect(snake.getPositionsAsString()).toEqual(expectedString);
        snake.right();

        expectedString = '.....\n' +
            '..##.\n' +
            '..#..\n' +
            '..#..\n' +
            '.....';
        snake.move(true);
        expect(snake.getPositionsAsString()).toEqual(expectedString);

        snake = new Snake(3);
        expectedString = '...\n' +
            '##.\n' +
            '.#.';
        snake.left();
        snake.move();

        expect(snake.getPositionsAsString()).toEqual(expectedString);
        expectedString = '...\n' +
            '##.\n' +
            '#..';
        snake.down();
        snake.move();
        expect(snake.getPositionsAsString()).toEqual(expectedString);
    });

    describe('Movement', () => {
        it('should move correct', () => {
            const oldPos = snake.positions[0];

            snake.move();
            expect(snake.positions[0]).toEqual(oldPos - 5);

            snake.move();
            expect(snake.positions[0]).toEqual(oldPos - 10);

            snake.move();
            expect(snake.positions[0]).toEqual(oldPos + 10);

            snake.move();
            expect(snake.positions[0]).toEqual(oldPos + 5);
        });

        it('should return weather moved or not because of self collision', () => {
            let moved: boolean;

            snake.right();
            moved = snake.move(true);
            expect(moved).toEqual(true);

            snake.down();
            moved = snake.move(true);
            expect(moved).toEqual(true);

            snake.left();
            moved = snake.move();
            expect(moved).toEqual(false);

            snake.down();
            moved = snake.move();
            expect(moved).toEqual(true);
        });

        it('should move and grow', () => {
            const oldHead = snake.head();
            const oldTail = snake.tail();
            const oldSize = snake.positions.length;

            snake.move(true);
            expect(snake.head()).toEqual(oldHead - 5);
            expect(snake.positions.length).toEqual(oldSize + 1);
            expect(snake.tail()).toEqual(oldTail);
        });

        it('should change direction correct', () => {

            snake.left();
            snake.move();
            expect(snake.positions[0]).toEqual(11);
            expect(snake.positions[1]).toEqual(12);
            expect(snake.positions[2]).toEqual(17);

            snake.up();
            snake.move();
            expect(snake.positions[0]).toEqual(6);
            expect(snake.positions[1]).toEqual(11);
            expect(snake.positions[2]).toEqual(12);

            snake.right();
            snake.move();
            expect(snake.positions[0]).toEqual(7);
            expect(snake.positions[1]).toEqual(6);
            expect(snake.positions[2]).toEqual(11);

            snake.down();
            snake.move();
            expect(snake.positions[0]).toEqual(12);
            expect(snake.positions[1]).toEqual(7);
            expect(snake.positions[2]).toEqual(6);

            snake.up();
            snake.move();
            expect(snake.positions[0]).toEqual(17);
            expect(snake.positions[1]).toEqual(12);
            expect(snake.positions[2]).toEqual(7);

            snake.left();
            snake.move();
            snake.right();
            snake.move();
            expect(snake.positions[0]).toEqual(15);

            snake.up();
            snake.move();
            snake.down();
            snake.move();
            snake.right();
            snake.move();
            snake.left();
            snake.move();
            expect(snake.positions[0]).toEqual(7);
        });

        it('should handle move on edges correct', () => {
            snake = new Snake(3);

            snake.move();
            expect(snake.positions[0]).toEqual(1);

            snake.move();
            expect(snake.positions[0]).toEqual(7);

            snake.left();
            snake.move();
            snake.move();
            expect(snake.positions[0]).toEqual(8);

            snake.down();
            snake.move();
            expect(snake.positions[0]).toEqual(2);

            snake.right();
            snake.move();
            expect(snake.positions[0]).toEqual(0);
        });
    });

    describe('Body', () => {
        it('should initially have a body', () => {
            expect(snake.positions.length).toEqual(3);
            expect(snake.positions[0]).toEqual(12);
            expect(snake.positions[1]).toEqual(17);
            expect(snake.positions[2]).toEqual(22);
        });
    });

});
