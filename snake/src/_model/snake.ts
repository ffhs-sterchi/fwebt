import {Coordinate} from './coordinate';
import {mod} from './mod';

export class Snake {
    get positions(): number[] {
        return this._positions;
    }

    private readonly _positions: number[];
    private readonly planeSize: number;
    private readonly initialSnakeSize = 3;
    private direction: 'L' | 'R' | 'U' | 'D' = 'U';

    constructor(planeSize) {
        this.planeSize = planeSize;
        this._positions = [Math.floor(Math.pow(this.planeSize, 2) / 2)];
        for (let i = 1; i < this.initialSnakeSize; i++) {
            this._positions.push(
                this.addCoordinate(this._positions[0], i, 0)
            );
        }
    }

    tail(): number {
        return this.positions[this.positions.length - 1];
    }

    head(): number {
        return this.positions[0];
    }

    /**
     * Adds a number of rows od cols to a given number and returns the corresponding number back
     *
     * Example:
     * The `planeSize` is 5x5 big, the start number is 1.*
     * Now we add 1 row and 0 col, that would result in a return value of 6
     *
     * For better visualisation the plane:
     * | 0  1  2  3  4|
     * | 5  6  7  8  9|
     * |10 11 12 13 14|
     * |15 16 17 18 19|
     * |20 21 22 23 24|
     *
     * @param number
     * @param addRow
     * @param addCol
     */
    private addCoordinate(number: number, addRow: number, addCol: number): number {
        const {row, col} = this.numberToCoordinates(number);
        return this.coordinatesToNumber({
            col: mod(col + addCol, this.planeSize),
            row: mod(row + addRow, this.planeSize),
        });
    }

    numberToCoordinates(number: number): Coordinate {
        if (number < 0 || number >= Math.pow(this.planeSize, 2)) {
            return null;
        }
        const col = (number % this.planeSize);
        const row = Math.floor(number / this.planeSize);

        return {row, col};
    }

    coordinatesToNumber(coordinates: Coordinate): number {
        const number = (coordinates.row * this.planeSize) + coordinates.col;
        return number < Math.pow(this.planeSize, 2) ? number : null;
    }

    hasSnake(fieldNo: number): boolean {
        return this._positions.indexOf(fieldNo) >= 0;
    }

    move(shouldGrow = false): boolean {
        let newHeadNumber: number = null;
        const headNumber: number = this._positions[0];

        switch (this.direction) {
            case 'U':
                newHeadNumber = this.addCoordinate(headNumber, -1, 0);
                break;
            case 'D':
                newHeadNumber = this.addCoordinate(headNumber, 1, 0);
                break;
            case 'L':
                newHeadNumber = this.addCoordinate(headNumber, 0, -1);
                break;
            case 'R':
                newHeadNumber = this.addCoordinate(headNumber, 0, 1);
                break;
        }

        const collidesSelf = this.positions.indexOf(newHeadNumber);
        const collidesNewTail = !shouldGrow && newHeadNumber !== this.tail();
        if (collidesNewTail && collidesSelf >= 0) {
            return false;
        }

        if (!shouldGrow) {
            this._positions.pop();
        }
        this._positions.unshift(newHeadNumber);
        return true;
    }

    left(): void {
        if (this.direction !== 'R') {
            this.direction = 'L';
        }
    }

    right(): void {
        if (this.direction !== 'L') {
            this.direction = 'R';
        }
    }

    up(): void {
        if (this.direction !== 'D') {
            this.direction = 'U';
        }
    }

    down(): void {
        if (this.direction !== 'U') {
            this.direction = 'D';
        }
    }

    getPositionsAsString(): string {
        let c = 0;
        return Array(this.planeSize).fill(Array(this.planeSize).fill('.'))
            .map(row =>
                row.map(v =>
                    this.positions.includes(c++) ? '#' : v
                ).join('')
            ).join('\n');
    }
}
