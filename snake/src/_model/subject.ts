export class Subject<T> {
    private _value;
    private readonly observers: ((value: T) => void)[] = [];

    constructor(initialValue?: T) {
        this._value = initialValue;
    }

    public next(value: T): void {
        this._value = value;
        this.observers.forEach(c => c(value));
    }

    public subscribe(callback: (value: T) => void): void {
        this.observers.push(callback);
        callback(this._value);
    }

    public get value(): T {
        return this._value;
    }
}
