/**
 * Is needed because of the behaviour of Mod in JS. It respects minus numbers "correct" see:
 * https://stackoverflow.com/questions/4467539/javascript-modulo-gives-a-negative-result-for-negative-numbers
 * @param n - The number to take the modulo of
 * @param m - The modus
 * @return The modulo value (n % m)
 */
export const mod = (n: number, m: number): number => (((n % m) + m) % m);
