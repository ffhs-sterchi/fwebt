import {Snake} from './snake';
import {Field} from './field';
import {Subject} from "./subject";

export interface GameState {
    size: number;
    snake: Snake;

    score?: Subject<number>;
    gameOver?: Subject<boolean>;
    getPlane?(): Field[][];
}
