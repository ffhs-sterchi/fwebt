import React, {Component} from 'react';
import Game from './game/game';
import {Snake} from './_model/snake';
import {GameHandler} from './_service/game-handler';

class App extends Component<{}, GameHandler> {
    private readonly gameHandler: GameHandler;

    constructor(props?) {
        super(props);
        const size = 31;
        const snake = new Snake(size);
        this.gameHandler = new GameHandler({size, snake});
        this.state = this.gameHandler;
    }

    render() {
        return (
            <div id="App">
                <Game handler={this.state}/>
            </div>
        );
    }
}

export default App;
