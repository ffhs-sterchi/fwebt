import React, {Component} from 'react';
import Score from './score/score';
import Plane from './plane/plane';
import './game.scss';
import {GameState} from '../_model/game-state';
import GameOverMessage from './game-over-message/game-over-message';
import {GameOverAdapter} from '../_service/game-over.adapter';

class Game extends Component<{ handler: GameState }, { gameOver: boolean }> {
    constructor(props) {
        super(props);
        this.state = {gameOver: false};
    }

    componentDidMount(): void {
        this.props.handler.gameOver.subscribe(gameOver => this.setState({gameOver}));
    }

    render() {
        return (
            <div className="sn-game">
                <Score handler={this.props.handler}/>
                <Plane fields={this.props.handler.getPlane()}/>
                {this.state.gameOver ? <GameOverMessage saveFn={this.save.bind(this)}/> : ''}
            </div>
        );
    }

    save(name: string): void {
        GameOverAdapter.instance().save({
            name,
            score: this.props.handler.score.value
        });
    }
}

export default Game;
