import React from 'react';
import '@testing-library/jest-dom';
import {act} from 'react-dom/test-utils';
import {render} from 'react-dom';
import {container} from '../../testing/container-test';
import Score from './score';
import {TestUtil} from '../../testing/util';
import {GameHandler} from '../../_service/game-handler';

describe('Score as Component', () => {
    it('should show initial score', () => {
        act(() => {
            render(<Score handler={TestUtil.aGameHandler()}/>, container);
        });
        expect(document.querySelectorAll('#score').length).toEqual(1);
        expect(document.querySelector('#score').textContent).toEqual('Score: 0');
    });

    it('should show correct score', () => {
        spyOn(Math, 'random').and.returnValue(0.28);
        const handler = new GameHandler(TestUtil.aGameState(5));
        handler.tick();
        act(() => {
            render(<Score handler={handler}/>, container);
        });
        expect(document.querySelector('#score').textContent).toEqual('Score: 1');
    });
});
