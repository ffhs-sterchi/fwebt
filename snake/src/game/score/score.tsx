import React, {Component} from 'react';
import './score.scss';
import {GameState} from '../../_model/game-state';

class Score extends Component<{ handler: GameState }, { score: number }> {
    constructor(props) {
        super(props);
        this.state = {score: 0};
    }

    componentDidMount(): void {
        this.props.handler.score.subscribe(score => this.setState({score}));
    }

    render() {
        return (
            <div className="sn-score">
                <div id="score">Score: {this.state.score}</div>
            </div>
        );
    }
}

export default Score;
