import React from 'react';
import '@testing-library/jest-dom';
import Game from './game';
import {act} from 'react-dom/test-utils';
import {render} from 'react-dom';
import {container, countSelector} from '../testing/container-test';
import {TestUtil} from '../testing/util';
import {GameOverAdapter} from '../_service/game-over.adapter';
import {GameHandler} from '../_service/game-handler';

describe('Game as Component', () => {
    const state = TestUtil.aGameHandler();
    const renderComponent = (): void => {
        act(() => {
            render(<Game handler={state}/>, container);
        });
    };
    it('should render the Game correct', () => {
        renderComponent();
        expect(countSelector('.sn-game')).toEqual(1);
        expect(countSelector('.sn-square.has-food')).toEqual(1);
        expect(countSelector('.sn-square.has-snake')).toEqual(3);
        expect(countSelector('.sn-square.has-snake.has-food')).toEqual(0);
        expect(countSelector('.sn-game-over-message')).toEqual(0);
    });

    it('should display GameOverMessage on gameOver', () => {
        state.gameOver.next(true);
        renderComponent();
        expect(countSelector('.sn-game-over-message')).toEqual(1);
    });

    it('should display GameOverMessage on gameOver', () => {
        const gameHandler: GameHandler = TestUtil.aGameHandler();
        gameHandler.score.next(66);
        const game: Game = new Game({handler: gameHandler});
        spyOn(GameOverAdapter.instance(), 'save');

        game.save('bacon');

        expect(GameOverAdapter.instance().save).toHaveBeenCalledTimes(1);
        expect(GameOverAdapter.instance().save).toHaveBeenCalledWith({name: 'bacon', score: 66});
    });
});
