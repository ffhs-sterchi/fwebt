import React from 'react';
import '@testing-library/jest-dom';
import Plane from './plane';
import {act} from 'react-dom/test-utils';
import {render} from 'react-dom';
import {container, countSelector} from '../../testing/container-test';
import {Field} from '../../_model/field';
import {TestUtil} from '../../testing/util';

describe('Plane Component', () => {
    it('should render correct ammount of squares', () => {
        const fields: Field[][] = [
            [TestUtil.aField(0), TestUtil.aField(1)],
            [TestUtil.aField(2), TestUtil.aField(3)]
        ];
        act(() => {
            render(<Plane fields={fields}/>, container);
        });
        expect(countSelector('.sn-square')).toEqual(4);
    });
});
