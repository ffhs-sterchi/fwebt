import React, {Component} from 'react';
import Square from './square/square';
import './plane.scss';
import {Field} from '../../_model/field';


interface PlaneProps {
    fields: Field[][];
}

class Plane extends Component<PlaneProps> {

    render() {
        return (
            <div className="sn-plane">
                <div id="plane">
                    {this.getGrid()}
                </div>
            </div>
        );
    }

    getGrid() {
        return this.props.fields.map((row) =>
            <div key={Math.random()} className="plane-row">
                {row.map(field => <Square key={field.id} field={field}/>)}
            </div>
        );
    }
}

export default Plane;
