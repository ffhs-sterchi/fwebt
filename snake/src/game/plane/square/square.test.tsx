import React from 'react';
import '@testing-library/jest-dom';
import Square from './square';
import {act} from 'react-dom/test-utils';
import {render} from 'react-dom';
import {container, countSelector} from '../../../testing/container-test';
import {TestUtil} from '../../../testing/util';

describe('Square Component', () => {
    it('should render a empty square correct', () => {
        act(() => {
            render(<Square field={TestUtil.aField()}/>, container);
        });
        expect(countSelector('.has-food')).toEqual(0);
    });

    it('should render a food square correct', () => {
        act(() => {
            const field = TestUtil.aField();
            field.hasFood = true;
            render(<Square field={field}/>, container);
        });
        expect(countSelector('.has-food')).toEqual(1);
    });

    it('should subscribe to field and setState', () => {
        act(() => {
            const field = TestUtil.aField();
            render(<Square field={field}/>, container);
            field.next(true, false);
        });
        expect(countSelector('.has-food')).toEqual(1);
    });

});
