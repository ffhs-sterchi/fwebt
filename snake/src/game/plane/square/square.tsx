import * as React from 'react';
import {Component} from 'react';
import './square.scss';
import {Field} from '../../../_model/field';

interface SquareProps {
    field: Field;
}

class Square extends Component<SquareProps, Field> {
    public static defaultProps = {hasFood: false};

    constructor(props?) {
        super(props);
        this.state = this.props.field;
        this.props.field.subscribe(field => {
            this.setState(field);
        });
    }

    getClass(): string {
        const food = this.state.hasFood ? 'has-food' : '';
        const snake = this.state.hasSnake ? 'has-snake' : '';
        return `sn-square ${food} ${snake}`.trim();
    }

    render() {
        return (
            <div className={this.getClass()}>
                <div className='square'>
                </div>
            </div>
        );
    }
}

export default Square;
