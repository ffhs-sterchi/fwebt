import React from 'react';
import '@testing-library/jest-dom';
import {act} from 'react-dom/test-utils';
import {render} from 'react-dom';
import {container} from '../../testing/container-test';
import GameOverMessage from './game-over-message';
import {GameOverAdapter} from '../../_service/game-over.adapter';

describe('GameOverMessage as Component', () => {
    const onClickSpy = jest.fn();
    it('should show game over message', () => {
        act(() => {
            render(<GameOverMessage saveFn={onClickSpy}/>, container);
        });
        expect(document.querySelectorAll('.sn-game-over-message .message').length).toEqual(1);
        expect(document.querySelector('.sn-game-over-message h2').textContent).toEqual('Game Over');
        expect(document.querySelector('.sn-game-over-message label').textContent).toEqual('Dein Name');
        expect(document.querySelector('.sn-game-over-message input[type="text"][name="name"')).toBeTruthy();
        expect(document.querySelector('.sn-game-over-message button').textContent).toEqual('speichern');
    });

    it('should show bestlist', () => {
        GameOverAdapter.instance().latest.next([
            {score: 55, name: 'lorem'},
            {score: 22, name: 'ipsum'}
        ]);
        act(() => {
            render(<GameOverMessage saveFn={onClickSpy}/>, container);
        });
        expect(document.querySelectorAll('.score-entry').length).toEqual(2);
    });

    it('should call save function', () => {
        act(() => {
            render(<GameOverMessage saveFn={onClickSpy}/>, container);
            const input: HTMLInputElement = document.querySelector('.sn-game-over-message input');
            input.value = 'bacon';
            const button: HTMLButtonElement = document.querySelector('#save-button');
            button.click();
        });

        expect(onClickSpy).toHaveBeenCalledTimes(1);
        expect(onClickSpy).toHaveBeenCalledWith('bacon');
    });
});
