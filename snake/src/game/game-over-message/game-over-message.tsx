import React, {Component} from 'react';
import './game-over-message.scss';
import {GameResult} from '../../_model/game-result';
import {GameOverAdapter} from '../../_service/game-over.adapter';

interface GameOverMessageProps {
    saveFn(name: string): void;
}

interface GameOverMessageState {
    name: string;
    bestlist: GameResult[];
}

class GameOverMessage extends Component<GameOverMessageProps, GameOverMessageState> {
    constructor(props) {
        super(props);
        this.state = {name: '', bestlist: []};
    }

    componentDidMount() {
        GameOverAdapter.instance().latest.subscribe(latest =>
            this.setState({...this.state, bestlist: latest})
        );
    }

    render() {
        return (
            <div className="sn-game-over-message">
                <div className="message">
                    <h2>Game Over</h2>
                    {this.state.name === '' ?
                        <div className="score-save-form">
                            <label>Dein Name</label>
                            <input type="text" name="name"/>
                            <button onClick={this.save.bind(this)} id="save-button">speichern</button>
                        </div> : ''}
                    {this.state.bestlist.length > 0 ? this.renderBestlist() : ''}

                </div>
            </div>
        );
    }

    renderBestlist() {
        return this.state.bestlist.map(score =>
            <div className="score-entry" key={score.name + score.score}>{score.name} ({score.score})</div>
        );
    }

    save(): void {
        const name = (document.querySelector('.sn-game-over-message input') as HTMLInputElement).value;
        this.props.saveFn(name);
        this.setState({
            name
        });
    }
}

export default GameOverMessage;
