import React from 'react';
import {render} from 'react-dom';
import {act} from 'react-dom/test-utils';
import '@testing-library/jest-dom';

import App from './app';
import {container, countSelector} from './testing/container-test';


describe('App', () => {
    it('renders App, Game, Plane, Score, Square', () => {
        act(() => {
            render(<App/>, container);
        });
        expect(container.textContent).toContain('Score');
        expect(document.querySelector('#App')).toBeTruthy();
        expect(countSelector('.sn-game')).toEqual(1);
        expect(countSelector('.sn-score')).toEqual(1);
        expect(countSelector('.sn-plane')).toEqual(1);
        expect(countSelector('.sn-square')).toBeGreaterThan(1);
    });

    it('should setInterval', () => {
        spyOn(window, 'setInterval');
        act(() => {
            render(<App/>, container);
        });
        expect(window.setInterval).toHaveBeenCalledTimes(1);
    });
});
