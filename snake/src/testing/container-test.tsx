import {unmountComponentAtNode} from 'react-dom';
import '@testing-library/jest-dom';
import React from 'react';

export let container: Element = null;

export function countSelector(selector: string): number {
    return document.querySelectorAll(selector).length;
}

beforeEach(() => {
    // setup a DOM element as a render target
    container = document.createElement('div');
    document.body.appendChild(container);
});

afterEach(() => {
    // cleanup on exiting
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});
