import {Snake} from '../_model/snake';
import {GameHandler} from '../_service/game-handler';
import {GameState} from '../_model/game-state';
import {Field} from '../_model/field';

export class TestUtil {
    static aGameHandler(): GameHandler {
        return new GameHandler(TestUtil.aGameState());
    }

    static aGameState(size?: number): GameState {
        return {
            size: size ? size : 31,
            snake: new Snake(size ? size : 31)
        };
    }

    static aField(id = 0): Field {
        const field = new Field();
        field.id = id;
        field.hasFood = false;
        field.hasSnake = false;
        return field;
    }
}
