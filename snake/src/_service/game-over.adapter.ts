import firebase from 'firebase';
import {GameResult} from '../_model/game-result';
import {Subject} from '../_model/subject';

export class GameOverAdapter {
    private static _instance: GameOverAdapter = new GameOverAdapter();
    private readonly firebaseConfig = {
        apiKey: 'AIzaSyCbisCSkrIdf9o_ofnF0UBq02PcFKjprvA',
        authDomain: 'snake-fwebt.firebaseapp.com',
        databaseURL: 'https://snake-fwebt.firebaseio.com',
        projectId: 'snake-fwebt',
        storageBucket: 'snake-fwebt.appspot.com',
        messagingSenderId: '267529809805',
        appId: '1:267529809805:web:303893509dbf80e71997c4'
    };
    private readonly db: firebase.firestore.Firestore;
    public latest: Subject<GameResult[]> = new Subject<GameResult[]>([]);

    private constructor() {
        firebase.initializeApp(this.firebaseConfig);
        this.db = firebase.firestore();
    }

    static instance(): GameOverAdapter {
        return GameOverAdapter._instance;
    }

    private _collectionPath = 'scores';

    save(result: GameResult): void {
        if (result.name) this.db.collection(this._collectionPath).add(result).then(() => this.loadLatest());
    }

    private loadLatest(): void {
        this.db.collection(this._collectionPath).orderBy('score', 'desc').limit(6).get().then(val =>
            this.latest.next(
                val.docs.map(v =>
                    (v.data() as GameResult))
            )
        );
    }
}
