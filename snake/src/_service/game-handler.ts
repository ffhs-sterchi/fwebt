import {GameState} from '../_model/game-state';
import {Snake} from '../_model/snake';
import {EventHandler} from '../_model/event-handler';
import {Field} from '../_model/field';
import {Subject} from "../_model/subject";

export class GameHandler implements GameState {

    public readonly score: Subject<number> = new Subject(0);
    public readonly gameOver: Subject<boolean> = new Subject(false);
    private tempo = 200;
    private foodPosition: number;
    readonly snake: Snake;
    readonly size: number;
    private _ticker;
    private _fields: Field[];
    private eatenFoods: number[] = [];

    constructor(state: GameState) {
        this.size = state.size;
        this.snake = state.snake;
        this.initFields();
        this.handleControl();
        this.startTicker();
    }

    getPlane(): Field[][] {
        return Array(this.size).fill(0).map((value, index) =>
            this._fields.slice(this.size * index, this.size * (index + 1))
        );
    }

    getPlaneAsString(): string {
        return this.getPlane().map(row => {
            return row.map(field => {
                return field.hasSnake ? '#' : field.hasFood ? 'X' : '.';
            }).join('');
        }).join('\n');
    }

    startTicker(): void {
        this._ticker = setInterval(() => this.tick(), this.tempo);
    }

    stopTicker(): void {
        clearInterval(this._ticker);
    }

    tick(): void {
        const oldTail = this.snake.tail();

        const didMove = this.snake.move(this.hasDigest());
        if (!didMove) {
            this.triggerGameOver();
        }

        if (this.snake.head() === this.foodPosition) {
            this.eat();
        }

        this.setSnake(this.snake.head(), true);
        if (!this.snake.positions.includes(oldTail)) {
            this.setSnake(oldTail, false);
        }

        const field = this._fields[this.foodPosition];
        field.next(true, field.hasSnake);
    }

    private triggerGameOver(): void {
        this.stopTicker();
        this.gameOver.next(true);
    }

    private setSnake(pos: number, hasSnake: boolean): void {
        const field = this._fields[pos];
        field.next(field.hasFood, hasSnake);
    }

    private initFields(): void {
        this.foodPosition = this.randomFoodPosition();
        this._fields = [];
        for (let i = 0; i < (this.size * this.size); i++) {
            const field = new Field();
            field.id = i;
            field.next(field.id === this.foodPosition, this.snake.hasSnake(field.id));
            this._fields.push(field);
        }
    }

    private handleControl(): void {
        EventHandler.getHandler().subscribeKeydown(event => {
            switch (event.key.toUpperCase()) {
                case 'ArrowLeft'.toUpperCase():
                case 'A':
                    this.snake.left();
                    break;
                case 'ArrowRight'.toUpperCase():
                case 'D':
                    this.snake.right();
                    break;
                case 'ArrowUp'.toUpperCase():
                case 'W':
                    this.snake.up();
                    break;
                case 'ArrowDown'.toUpperCase():
                case 'S':
                    this.snake.down();
                    break;
            }
        });
    }

    private randomFoodPosition(): number {
        const position = Math.floor(
            Math.random() * Math.pow(this.size, 2)
        );
        return this.snake.hasSnake(position) ? position + 1 : position;
    }

    private eat() {
        this.eatenFoods.push(this.foodPosition);
        this.setFood(this.foodPosition, false);
        this.foodPosition = this.randomFoodPosition();
        this.setFood(this.foodPosition, true);
        this.score.next(this.score.value + 1);
    }

    private setFood(pos: number, hasFood: boolean): void {
        const field = this._fields[pos];
        field.next(hasFood, field.hasSnake);
    }


    private hasDigest(): boolean {
        const foodToDigest = this.eatenFoods.indexOf(this.snake.tail());
        if (foodToDigest >= 0) {
            this.eatenFoods.splice(foodToDigest, 1);
            return true;
        }
        return false;
    }

}
