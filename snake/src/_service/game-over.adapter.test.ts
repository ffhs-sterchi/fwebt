import '@testing-library/jest-dom';
import {GameOverAdapter} from './game-over.adapter';

jest.useFakeTimers();
describe('GameOverAdapter', () => {
    const sut: GameOverAdapter = GameOverAdapter.instance();
    it('should be a singleton', () => {
        expect(sut).not.toBeNull();
        expect(sut).not.toBeUndefined();
        expect(sut).toBeInstanceOf(GameOverAdapter);
        expect(GameOverAdapter.instance()).toBe(sut);
    });

    it('should save score', () => {
        sut.save({name: 'Some Badass Player', score: 9001});
    });
});
