import '@testing-library/jest-dom';
import {GameHandler} from './game-handler';
import {TestUtil} from '../testing/util';
import {EventHandler} from '../_model/event-handler';
import {Field} from '../_model/field';

jest.useFakeTimers();
describe('GameHandler', () => {
    let gameHandler: GameHandler;
    let keydownCallback: (event: KeyboardEvent) => void;
    const eventHandler = EventHandler.getHandler();
    const fields = (handler?: GameHandler): Field[] => [].concat(...(handler ? handler : gameHandler).getPlane());
    const foodField = (handler?: GameHandler): Field => {
        const foodFields = fields(handler).filter(f => f.hasFood);
        expect(foodFields.length).toEqual(1);
        return foodFields[0];
    };

    beforeEach(() => {
        spyOn(eventHandler, 'subscribeKeydown').and.callFake(callback => {
            keydownCallback = callback;
        });
        gameHandler = TestUtil.aGameHandler();
    });

    it('should set food position', () => {
        expect(foodField()).toBeTruthy();
        expect(foodField().id).toBeLessThanOrEqual(Math.pow(gameHandler.size, 2));
        expect(gameHandler.snake).not.toContain(foodField().id);
    });

    it('should set food position correct when same as snake', () => {
        spyOn(Math, 'random').and.returnValue(0.5);
        const handler = new GameHandler(TestUtil.aGameState(5));
        expect(foodField(handler).id).toEqual(13);
    });

    it('should handle a tick', () => {
        spyOn(gameHandler.snake, 'move');
        gameHandler.startTicker();

        expect(setInterval).toHaveBeenCalled();
        expect(setInterval).toHaveBeenLastCalledWith(expect.any(Function), 200);

        jest.runOnlyPendingTimers();
        expect(gameHandler.snake.move).toHaveBeenCalled();
    });

    it('should handle eating of food', () => {
        spyOn(Math, 'random').and.returnValue(0.28);
        const handler = new GameHandler(TestUtil.aGameState(5));
        expect(foodField(handler).id).toEqual(7);
        spyOn(handler.snake, 'move').and.callThrough();

        handler.tick();
        expect(foodField(handler).id).toEqual(8);
        handler.tick();
        handler.tick();
        handler.tick();
        handler.tick();

        expect(handler.snake.move).toHaveBeenCalledTimes(5);
        expect(handler.snake.move).toHaveBeenNthCalledWith(1, false);
        expect(handler.snake.move).toHaveBeenNthCalledWith(2, false);
        expect(handler.snake.move).toHaveBeenNthCalledWith(3, false);
        expect(handler.snake.move).toHaveBeenNthCalledWith(4, true);
        expect(handler.snake.move).toHaveBeenNthCalledWith(5, false);
    });

    it('should notify eat watchers on eat', () => {
        spyOn(Math, 'random').and.returnValue(0.28);
        const handler = new GameHandler(TestUtil.aGameState(5));

        let size = null;
        const callback = (newSize: number): void => {
            size = newSize;
        };

        expect(size).toBeNull();
        handler.score.subscribe(callback);
        expect(size).toEqual(0);
        handler.tick();
        expect(size).toEqual(1);
    });

    it('should get plane as string', () => {
        spyOn(Math, 'random').and.returnValue(0.28);
        const handler = new GameHandler(TestUtil.aGameState(5));

        let expectedPlane = '.....\n' +
            '..X..\n' +
            '..#..\n' +
            '..#..\n' +
            '..#..';
        expect(handler.getPlaneAsString()).toEqual(expectedPlane);

        handler.tick();
        expectedPlane = '.....\n' +
            '..#X.\n' +
            '..#..\n' +
            '..#..\n' +
            '.....';
        expect(handler.getPlaneAsString()).toEqual(expectedPlane);
    });

    it('should stop timer', () => {
        spyOn(gameHandler.snake, 'move');
        gameHandler.startTicker();
        gameHandler.stopTicker();

        expect(clearInterval).toHaveBeenCalled();
        expect(setInterval).toHaveBeenCalled();
        expect(setInterval).toHaveBeenLastCalledWith(expect.any(Function), 200);
    });

    it('should trigger gameOver when snake did not move anymore', () => {
        spyOn(gameHandler.snake, 'move').and.returnValue(false);
        spyOn(gameHandler, 'stopTicker');
        let gameOver = null;
        gameHandler.gameOver.subscribe(v => gameOver = v);

        gameHandler.startTicker();
        expect(gameOver).toEqual(false);
        expect(gameHandler.stopTicker).not.toHaveBeenCalled();

        jest.runOnlyPendingTimers();

        expect(gameOver).toEqual(true);
        expect(gameHandler.stopTicker).toHaveBeenCalled();
    });

    it('should handle direction changes on WASD', () => {
        expect(eventHandler.subscribeKeydown).toHaveBeenCalledTimes(1);

        spyOn(gameHandler.snake, 'left');
        keydownCallback({key: 'A'} as KeyboardEvent);
        expect(gameHandler.snake.left).toHaveBeenCalledTimes(1);

        spyOn(gameHandler.snake, 'right');
        keydownCallback({key: 'D'} as KeyboardEvent);
        expect(gameHandler.snake.right).toHaveBeenCalledTimes(1);

        spyOn(gameHandler.snake, 'up');
        keydownCallback({key: 'W'} as KeyboardEvent);
        expect(gameHandler.snake.up).toHaveBeenCalledTimes(1);

        spyOn(gameHandler.snake, 'down');
        keydownCallback({key: 'S'} as KeyboardEvent);
        expect(gameHandler.snake.down).toHaveBeenCalledTimes(1);
    });

    it('should handle direction changes on ArrowKeys', () => {
        expect(eventHandler.subscribeKeydown).toHaveBeenCalledTimes(1);

        spyOn(gameHandler.snake, 'left');
        keydownCallback({key: 'ArrowLeft'} as KeyboardEvent);
        expect(gameHandler.snake.left).toHaveBeenCalledTimes(1);

        spyOn(gameHandler.snake, 'right');
        keydownCallback({key: 'ArrowRight'} as KeyboardEvent);
        expect(gameHandler.snake.right).toHaveBeenCalledTimes(1);

        spyOn(gameHandler.snake, 'up');
        keydownCallback({key: 'ArrowUp'} as KeyboardEvent);
        expect(gameHandler.snake.up).toHaveBeenCalledTimes(1);

        spyOn(gameHandler.snake, 'down');
        keydownCallback({key: 'ArrowDown'} as KeyboardEvent);
        expect(gameHandler.snake.down).toHaveBeenCalledTimes(1);
    });
});
