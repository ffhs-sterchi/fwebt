module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "jest/globals": true,
        "mocha": true
    },
    "extends": [
        "plugin:@typescript-eslint/recommended",
        "plugin:react/recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true,
            "legacyDecorators": true
        },
        "sourceType": "module",
        "allowImportExportEverywhere": false
    },
    "plugins": [
        "react",
        'jest'
    ],
    "rules": {
        "semi": ["error", "always"],
        "quotes": ["error", "single"]
    }
};
